@extends('layouts.app')

@section('content')
<div class="container ">
    <form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-8 offset-2">
         <div class="row">
             <h1>ADD NEW POST</h1>
         </div>
        <div class="form-group row pt-3">
            <label for="caption" class="col-md-4 col-form-label">{{ __('Post Caption') }}</label>

            
                <input id="caption" type="text" class="form-control @error('caption') is-invalid @enderror" name="caption" autocomplete="caption" autofocus>

                @error('caption')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                          
        </div>
                        
     <div class="form-group row pt-3">
        <label for="image" class="col-md-4 col-form-label">Post Image</label>
        <input type="file" class="form-control-file @error('caption') is-invalid @enderror" name="image" id="image">

        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>

        <div class="row pt-3">
            <button class="btn-btn-primary">Submit</button>
        </div>
    </div>
        
       
    </div>
   </form>
</div>
@endsection
