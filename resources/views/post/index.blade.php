@extends('layouts.app')

@section('content')
<div class="container ">
    @foreach( $posts as $post )
        <div class="row">
            <div class="col-6 offset-3">
                <div class="d-flex align-items-center">
                    <div class="pr-3 pb-3">
                        <img src="{{ $post->user->profile->profileImage() }}" class="rounded-circle w-100" 
                            style="max-width:50px; max-height:50px"   alt="">
                    </div>
                    <div class="font-weight-bold">
                        <a href="/profile/{{ $post->user->id }}">
                        <span class="text-dark">{{ $post->user->username }}</span></a>
                        <a href="#" class="pl-3">Follow</a>
                    </div>
                </div>
                    <img src="/storage/{{ $post->image }}" class="w-100" alt="">
                    
                
                    <p class="pt-2">
                        <span class="font-weight-bold">
                            <a href="/profile/{{ $post->user->id }}">
                            <span class="text-dark">{{ $post->user->username }}</span></a>
                            
                        </span>
                            {{ $post->caption }}
                    </p>
                
            </div>
            
        </div>
    @endforeach
    {{-- <div class="row">
        <div class="col-12 d-flex justify-content-center"> 
            {{ $posts->links() }}
        </div>
    </div> --}}
</div>
@endsection
