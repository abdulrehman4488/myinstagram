@extends('layouts.app')

@section('content')
<div class="container ">
    <div class="row">
        <div class="col-6">
            <img src="/storage/{{ $post->image }}" class="w-100" alt="">
        </div>
        <div class="col-6">
            <div class="d-flex align-items-center">
                <div class="pr-3 pb-3">
                    <img src="{{ $post->user->profile->profileImage() }}" class="rounded-circle w-100" 
                        style="max-width:50px; max-height:50px"   alt="">
                 </div>
                 <div class="font-weight-bold">
                    <a href="/profile/{{ $post->user->id }}">
                    <span class="text-dark">{{ $post->user->username }}</span></a>
                    <a href="#" class="pl-3">Follow</a>
                 </div>
            </div>
                <p>
                    <span class="font-weight-bold">
                        <a href="/profile/{{ $post->user->id }}">
                        <span class="text-dark pr-3">{{ $post->user->username }}</span></a>
                        
                    </span>
                        {{ $post->caption }}
               </p>
        </div>
    </div>
</div>
@endsection
