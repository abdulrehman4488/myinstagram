@extends('layouts.app')

@section('content')
<div class="container ">
 <form action="/profile/{{ $user->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method("PATCH")
    <div class="row">
        <div class="col-8 offset-2">
         <div class="row">
             <h1>Edit Profile</h1>
         </div>
        <div class="form-group row pt-3">
            <label for="title" class="col-md-4 col-form-label">{{ __('Profile title') }}</label>

            
                <input id="title" type="text" 
                                  class="form-control @error('title') is-invalid @enderror" 
                                  name="title" 
                                  value="{{ $user->profile->title }}"
                                  autocomplete="title" autofocus>

                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                          
        </div>

        <div class="form-group row pt-3">
            <label for="description" class="col-md-4 col-form-label">{{ __('Profile description') }}</label>

            
                <input id="description" type="text" 
                                        class="form-control @error('description') is-invalid @enderror" 
                                        name="description" 
                                        value="{{ $user->profile->description }}"
                                        autocomplete="description" autofocus>

                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                          
        </div>

        <div class="form-group row pt-3">
            <label for="url" class="col-md-4 col-form-label">{{ __('Profile url') }}</label>

            
                <input id="url" type="text" 
                                class="form-control @error('url') is-invalid @enderror" 
                                name="url" 
                                value="{{ $user->profile->url }}"
                                autocomplete="url" autofocus>

                @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                          
        </div>
                        
     <div class="form-group row pt-3">
        <label for="image" class="col-md-4 col-form-label">Profile Image</label>
        <input type="file" class="form-control-file @error('caption') is-invalid @enderror" name="image" id="image">

        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>

        <div class="row pt-3">
            <button class="btn-btn-primary">Update Profile</button>
        </div>
    </div>
        
       
    </div>
   </form>
</div>
@endsection
