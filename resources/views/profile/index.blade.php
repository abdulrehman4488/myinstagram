@extends('layouts.app')

@section('content')
<div class="container ">
    <div class="row d-flex">
        <div class="col-3 p-5">
        <img src="{{ $user->profile->profileImage() }}" class="img-fluid rounded-circle" alt="">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>{{ $user->username }}</h1>
                <follow-component user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-component>
                @can('update', $user->profile)
                    <a href="/post/create" >Add New Post</a>
                @endcan
            </div>
            @can('update', $user->profile)
                <a href="/profile/{{ $user->id }}/edit" >Edit Profile</a>
            @endcan
            <div class="d-flex pt-2">
                <div class="pr-5"><strong>{{ $user->posts->count() }}</strong> posts</div>
                <div class="pr-5"><strong>{{ $user->profile->followers->count() }}</strong> followers</div>
                <div class="pr-5"><strong>{{ $user->following->count() }}</strong> following</div>
            </div>
            <div class="font-weight-bold pt-3">{{ $user->profile->title }}</div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="#">{{ $user->profile->url }}</div>
        </div>
    </div>
    <div class="row" >
        @foreach ($user->posts as $posts)
            <div class="col-4 pt-4">
                <a href="/post/{{ $posts->id }}">
                    <img class="w-100" src="/storage/{{ $posts->image }}" alt="">
                </a>
            </div>
        @endforeach
        
    </div>
</div>
@endsection
