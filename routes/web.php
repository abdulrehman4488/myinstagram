<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::post('/follow/{user}', [App\Http\Controllers\FollowController::class, 'store']);

Route::get('/', [App\Http\Controllers\PostController::class, 'index']);
Route::get('/post/create', [App\Http\Controllers\PostController::class, 'create'])->name('Post.create');
Route::post('/post', [App\Http\Controllers\PostController::class, 'store'])->name('Post.store');
Route::get('/post/{post}', [App\Http\Controllers\PostController::class, 'show'])->name('Post.show');


Route::get('/profile/{user}', [App\Http\Controllers\ProfileController::class, 'index'])->name('Profile.show');
Route::get('/profile/{user}/edit', [App\Http\Controllers\ProfileController::class, 'edit'])->name('Profile.edit');
Route::patch('/profile/{user}', [App\Http\Controllers\ProfileController::class, 'update'])->name('Profile.update');



